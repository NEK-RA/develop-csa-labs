package nek.ra.jshop;

import javax.persistence.*;

@Entity
@Table(name="animals")

public class Animal {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int id;
    
    @Column(length=32)
    public String name;
    
    @Column(length=10)
    public String type;
    
    @Column
    public int age;
    
    @Column
    public int price;
}
