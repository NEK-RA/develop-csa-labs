package nek.ra.jshop;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AnimalController {

    @Autowired
    AnimalRepository arepo;

    //работает
    @GetMapping("/animals")
    public List<Animal> getAnimals() {
        return arepo.findAll();
    }

    //работает
    @PostMapping("/animals")
    public Animal addAnimal(@RequestBody Animal animal) {
        arepo.save(animal);
        return animal;
    }

    //Работает
    @GetMapping("/animals/{id}")
    public Animal getAnimal(@PathVariable("id") int id){
        return arepo.findById(id).get();
    }

    //работает
    @GetMapping("/animals/type/{typename}")
    public List<Animal> getAnimalByType(@PathVariable("typename") String type){
        return arepo.findByTypeEquals(type);
    }

    //вроде норм работает
    @Transactional
    @PostMapping("/animals/{id}")
    public Animal changeAnimal(@PathVariable("id") int id, @RequestBody Animal animal){
        Animal old = arepo.findById(id).get();
        old.name = animal.name;
        old.age = animal.age;
        old.price = animal.price;
        old.type = animal.type;
        return arepo.findById(id).get();
    }

    //работает
    @DeleteMapping("/animals/{id}")
    public void removeAnimal(@PathVariable("id") int id){
        arepo.deleteById(id);
    }

}
