import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class server {
    public static void main(String[] args) throws IOException {
            ServerSocket server = new ServerSocket(5982);
            System.out.println("Server Started");
            Socket client = server.accept();
            DataInputStream in = new DataInputStream(client.getInputStream());
            DataOutputStream out = new DataOutputStream(client.getOutputStream());
            String message = in.readUTF();
            System.out.println("client message is: " + message);
            if (message.equals("hello")) {
                out.writeUTF("Lab work 1 completed!");
            } else {
                out.writeUTF("Your word ("+message+") is not a \"hello\"");
            }
            in.close();
            out.close();
            client.close();
    }
}
