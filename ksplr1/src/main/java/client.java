import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.io.IOException;
import java.util.Scanner;

public class client {
    public static void main(String[] args) throws IOException {
        Socket s = new Socket("localhost", 5982);
        DataInputStream in = new DataInputStream(s.getInputStream());
        DataOutputStream out = new DataOutputStream(s.getOutputStream());
        Scanner scan = new Scanner(System.in);
        String word = scan.nextLine();
        out.writeUTF(word);
        String m = in.readUTF();
        System.out.println("Server answer is: "+m);
        in.close();
        out.close();
        s.close();

    }
}
